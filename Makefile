INCDIR = inc
ROMDIR = rom
SRCDIR = src

ROMS = \
	   $(ROMDIR)/deadchnl.rom \
	   $(ROMDIR)/fizzbuzz.rom \
	   $(ROMDIR)/mac.rom \
	   $(ROMDIR)/smile.rom

INCS = \
	   $(INCDIR)/devices.tal

all: $(ROMS)
	ls -l $(ROMDIR)

$(ROMDIR)/%.rom : $(SRCDIR)/%.tal $(INCS) $(ROMDIR)
	uxnasm $< $@

$(ROMDIR):
	mkdir -p $(ROMDIR)

clean:
	rm -f $(ROMDIR)/*.rom
	rmdir $(ROMDIR)

.PHONY: all clean
